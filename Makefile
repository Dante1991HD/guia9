prefix=/usr/local
CC = g++

CFLAGS = -g -Wall
SRC = hash.cpp Hash.cpp Lista.cpp
OBJ = hash.o Hash.o Lista.o
APP = hash

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
