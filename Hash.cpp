#include <iostream>
#include "Hash.h"
#include "Nodo.h"
#include "Lista.h"

using namespace std;

/* Constructor por defecto */
Hash::Hash(){
}


/* Función Hash por modulo */
int Hash::modulo_hash(int clave, int primo){
    int pos;
    // Se ocupa el ultimo numero primo del tamaño ingresado por el usuario
    pos = (clave % primo);
    return pos;
}

/* Segunda función Hash por modulo, más uno a la clave y al resultado */
int Hash::modulo_hash_aux(int clave, int primo){
    int pos=0;
    // Se ocupa la formula deseada
    pos = ((clave+1) % primo) + 1;
    return pos;
}

/* Método lineal para el manejo de colisiones */
void Hash::prueba_lineal(int arreglo[], int tamanio, int numero, int primo){
    int aux, aux1;
    // Generamos la direccion
    aux = modulo_hash(numero, primo);
    // Si se cumplen las condiciones para que este en arreglo se imprime la clave y su posicion
    if((arreglo[aux] != '\0') && (arreglo[aux] == numero)){
        cout << "Clave encontrada en: " << aux + 1 << endl;
    // Sino ejecutamos la prueba lineal
    }else{
        // Hacemos al aux1 según lo q nos indica la prueba lineal
        aux1 = aux + 1;
        // Mientras no se cumplan las 4 condiciones se irá aumentando el aux1
        while((aux1<=tamanio) && (arreglo[aux1] != '\0') && (arreglo[aux1] != numero) && (aux1 != aux)){
            aux1 = aux1 + 1;
            // Si el aux1 llega a ser mas grande que el tamaño
            if(aux1 == (tamanio+1)){
                aux1 = 1;
            }
        }
        // Si la posicion esta vacio, se reasigna a esa posicion el numero
        if(arreglo[aux1] == '\0'){
            arreglo[aux1] = numero;
            cout << "La clave no se encuentra en el arreglo y fue agregado en: " << aux1 + 1 << endl;
        }
        // Sino escribimos donde esta la clave
        else{
            cout << "La clave esta en la posición: " << aux1 + 1 << endl;
        }
    }
}

/* Método de prueba cuadratica para el manejo de colisiones */
void Hash::prueba_cuadratica(int arreglo[], int tamanio, int numero, int primo){
    int aux, aux1, i;
    // Generamos la direccion
    aux = modulo_hash(numero, primo);
    // Si se cumplen las condiciones deseadas es porque se encuentra la clave
    if((arreglo[aux] != '\0') && (arreglo[aux] == numero)){
        // Se imprime donde esta la clave
        cout << "Clave encontrado en la posición: " << aux + 1 << endl;
    // Sino ejecutamos la prueba cuadratica
    }else{
        i = 1;
        // Se ocupa el aux1 como variable para asignar
        aux1 = (aux+(i*i));
        // Mientras no se cumplan las condiciones seguiremos en el bucle aumentando el i y el aux1
        // segun lo que indica la prueba cuadratica
        while((arreglo[aux1] != '\0') && (arreglo[aux1] != numero)){
            i++;
            aux1 = (aux + (i*i));
            // Si el aux1 es mas grande que el tamaño volvemos todo al principio
            if(aux1 > tamanio){
                i=0;
                aux1=1;
                aux=1;
            }
        }
        // Reubicamos el numero en el arreglo con el valor del aux1
        if(arreglo[aux1] == '\0'){
            cout << "Clave no encontrada, reubicada en: " << aux1 + 1 << endl;
            arreglo[aux1] = numero;
        // Sino encontramos la clave
        }else{
            cout << "Clave encontrada en posicion: " << aux1 + 1 <<endl;
        }
    }
}

/* Método de doble dirección para el manejo de colisiones */
void Hash::doble_direccion(int arreglo[], int tamanio, int numero, int primo){
    int aux, aux1;
    // Generamos la direccion
    aux = modulo_hash(numero, primo);
    // Si se cumplen las condiciones para que este en arreglo se imprime la clave y su posicion
    if((arreglo[aux] != '\0') && (arreglo[aux] == numero)){
        cout << "La clave esta en la posicion: " << aux + 1 << endl;
    // Sino ejecutamos la prueba de doble direccion
    }else{
        // Hacemos al aux1 la direccion del hash auxiliar
        aux1 = modulo_hash_aux(aux, primo);
        // Mientras no se cumplan las 4 condiciones
        while((aux1 <= tamanio) && (arreglo[aux1] != '\0') && (arreglo[aux1] != numero) && (aux1 != aux)){
            // El aux1 irá tomando direcciones segun el hash auxiliar
            aux1 = modulo_hash_aux(aux1, primo);
        }
        // Si el arreglo esta vacio y el aux1 es distinto al numero se agrega
        if((arreglo[aux1] == '\0') || (arreglo[aux1] != numero)){
            arreglo[aux1] = numero;
            cout << "La clave no se encuentra en el arreglo y se agregó en: " << aux1 + 1 << endl;
        }
        // Sino explicamos donde se encuentra el numero
        else{
            cout << "La clave se encuentra en la posicion: " << aux1 + 1 << endl;
        }
    }
}

// Metodo de encademiento
void Hash::encadenamiento(int arreglo[], int tamanio, int numero, int primo, Lista *lista){
    int aux;
    // Generamos los nodos que usaremos además de una lista de nodos
    Nodo *nodo = NULL;
    Nodo *nodo2[tamanio];
    // Recorremos el tamaño agregando nuevos nodos por defecto
    for(int i=0; i<tamanio; i++){
        nodo2[i] = new Nodo();
        nodo2[i]->sig = NULL;
        nodo2[i]->numero = '\0';
    }
    // LLamamos al metodo para transformar nuestro arreglo a los nodos
    transformar_arreglo(nodo2, arreglo, tamanio);
    // Generamos la direccion
    aux = modulo_hash(numero, primo);
    // Si se cumplen las condiciones para que este en arreglo se imprime la clave y su posicion
    if((arreglo[aux] != '\0') && (arreglo[aux] == numero)){
        cout << "La clave esta en la posicion: " << aux + 1 << endl;
    }
    // Sino ejecutamos la prueba de colisiones por encadenamiento
    else{
        // El nodo toma el valor que existe en el siguiente a la direccion
        nodo = nodo2[aux]->sig;
        // Se deben cumplir ambas condiciones
        while((nodo != NULL) && (nodo->numero != numero)){
            // Mientras se va haciendo al nodo apuntar al siguiente
            nodo = nodo->sig;
        }
        // Si el nodo es nulo no se encuentra
        if(nodo == NULL){
            cout << "Agregado a la lista" << endl;
            cout << "Lista en esa posición" << endl;
            lista->crear(numero);
            lista->imprimir();       
        }
        // Sino apunta a donde se encuentra
        else{
            cout << "La clave se encuentra en la posicion: " << aux + 1 << endl;
        } 
    }
    cout << "Lista en esa posición" << endl;
}

// Metodo que copia arreglo en arreglo indicado en metodo de encadenamiento a través de Nodos
void Hash::transformar_arreglo(Nodo *nodo2[],int arreglo[], int tamanio){
    // Recorremos el largo 
	for(int i=0; i<tamanio; i++){
        // Almacenamos en el nodo el numero que se encuentra en el arreglo
		nodo2[i]->numero = arreglo[i];
        // Y apuntamos el siguiente nodo a nulo
    	nodo2[i]->sig = NULL;
	}
}

// Metodo que imprime la lista que acompaña al nodo
void Hash::imprimir_lista(Nodo *nodo2[],int aux){
    Nodo *nodo_aux = NULL;
	nodo_aux = nodo2[aux];
    // Nos irá imprimiendo la lista que acompaña al numero colisionado
	while(nodo_aux != NULL){
        // Si el numero es distinto a 0, lo imprimimos
		if(nodo_aux->numero != 0){
            // Lo hacemos en un formato tipico de las listas enlazadas
			cout << "[" << nodo_aux->numero << "] ->";
			}
        // Pasamos a la siguiente posición del nodo
		nodo_aux = nodo_aux->sig;
	}
}