#ifndef NODO_H
#define NODO_H

// Se define estructura para nodo
typedef struct _Nodo{
	int numero = 0;
    struct _Nodo *sig;
} Nodo;
#endif
