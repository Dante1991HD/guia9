#include <iostream>
#include "Nodo.h"
#include "Lista.h"

using namespace std;

#ifndef HASH_H
#define HASH_H

/* Constructo header del objeto Hash.cpp */
class Hash{
    private:
    public:
        // Constructor por defecto
        Hash();
        // Modulos hash por división
        int modulo_hash(int clave, int primo);
        // Modulo hash derivado 
        int modulo_hash_aux(int clave, int primo);
        // Metodo para la prueba lineal
        void prueba_lineal(int arreglo[], int tamanio, int numero, int primo);
        // Metodo para la prueba cuadratica
        void prueba_cuadratica(int arreglo[], int tamanio, int numero, int primo);
        // Metodo para la prueba de doble direccion
        void doble_direccion(int arreglo[], int tamanio, int numero, int primo);
        // Metodo para la prueba de encadenamiento
        void encadenamiento(int arreglo[], int tamanio, int numero, int primo, Lista *lista);
        // Metodo que hace un copiado del arreglo
        void transformar_arreglo(Nodo *nodo2[],int arreglo[], int tamanio);
        // Metodo que imprime la lista que acompaña al numero colisionado
        void imprimir_lista(Nodo *nodo2[],int aux);
};  
#endif
