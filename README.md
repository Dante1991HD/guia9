# Guía IX UIII

## Descripción del programa 
El programa generará una tabla Hash de números de tamaño "N" ingresado por el usuario, este implementa: prueba lineal, prueba cuadratica, doble dirección Hash y encadenamiento como métodos para el manejo de colisiones, en cada paso, ingreso o busqueda, muestra el contenido del arreglo de la forma (Dato, Posición) y/o lista enlazada, además indica si ocurre una colisión, en que posición ocurre y el desazamiento final.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make
En caso de no contar con make:

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.

## Compilación y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando.
```
make
```
* Luego de completada la compilación utilice el siguiente comando.
```
./hash {L|C|D|E}
```
* Es de vital importancia que se agrege sólo una de las letras {L|C|D|E}, da igual si en mayusculas o minusculas

## Salidas

## Se muestra ejemplo con el método lineal
* El programa generará una salida como la siguiente:
* Se selecciona el metodo
```
./hash l
```
* El programa pide un tamaño para el arreglo
```
Ingrese el tamaño del arreglo
Tamaño: 5
```
* Se desplega el menú
```
----------------------------------------
              [ MENU ]
----------------------------------------
 [1] Agregar números al arreglo
 [2] Buscar número en el arreglo
 [3] Salir del programa
----------------------------------------
Opcion: 
```
## Opción 1
* Agregamos algunos datos
```
Opcion: 1
Numero: 1

----------------------------------------
--- ARREGLO: (Dato, Posición) ---
(*, 1)-(1, 2)-(*, 3)-(*, 4)-(*, 5)
----------------------------------------

Opcion: 1
Numero: 2

----------------------------------------
--- ARREGLO: (Dato, Posición) ---
(*, 1)-(1, 2)-(2, 3)-(*, 4)-(*, 5)
----------------------------------------

Opcion: 1
Numero: 3

----------------------------------------
--- ARREGLO: (Dato, Posición) ---
(*, 1)-(1, 2)-(2, 3)-(3, 4)-(*, 5)
----------------------------------------

Opcion: 1
Numero: 5

----------------------------------------
--- ARREGLO: (Dato, Posición) ---
(5, 1)-(1, 2)-(2, 3)-(3, 4)-(*, 5)
----------------------------------------
```
* El siguiente número genera una colisión, el programa utiliza el método y lo reubica.
```
Opcion: 1
Numero: 7
Colision en posicion: 3
La clave no se encuentra en el arreglo y fue agregado en: 5

----------------------------------------
--- ARREGLO: (Dato, Posición) ---
(5, 1)-(1, 2)-(2, 3)-(3, 4)-(7, 5)
----------------------------------------
```
## Opción 2
* Esta nos permite buscar de la siguiente forma:
```
----------------------------------------
              [ MENU ]
----------------------------------------
 [1] Agregar números al arreglo
 [2] Buscar número en el arreglo
 [3] Salir del programa
----------------------------------------
Opcion: 2
El arreglo se encuentra de la siguiente forma

----------------------------------------
--- ARREGLO: (Dato, Posición) ---
(5, 1)-(1, 2)-(2, 3)-(3, 4)-(7, 5)
----------------------------------------
Ingrese numero: 7
La clave esta en la posición: 5

----------------------------------------
--- ARREGLO: (Dato, Posición) ---
(5, 1)-(1, 2)-(2, 3)-(3, 4)-(7, 5)
----------------------------------------
```
## Opción 3
* Esta opción cierra el programa.
```
----------------------------------------
              [ MENU ]
----------------------------------------
 [1] Agregar números al arreglo
 [2] Buscar número en el arreglo
 [3] Salir del programa
----------------------------------------
Opcion: 3
Finalización del programa con éxito
```

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl
