#include <iostream>
#include "Hash.h"
#include "Lista.h"

using namespace std;

/* Dunción que contiene el menú */
int menu() {
  string line;
  // Se muestra al usuario las funcionalidad del programa
  cout << "----------------------------------------" << endl;
  cout << "              [ MENU ]" << endl;
  cout << "----------------------------------------" << endl;
  cout << " [1] Agregar números al arreglo" << endl;
  cout << " [2] Buscar número en el arreglo" << endl;
  cout << " [3] Salir del programa" << endl;
  cout << "----------------------------------------" << endl;
  cout << "Opcion: ";
  getline(cin, line);
  // Se retorna la opción elegida
  return stoi(line);
}

/* Función que valida el metodo requerido por el usuario
para manejar las colisiones */
bool comprobar_metodo(char metodo_colision) {
  // Se crea un arreglo con todos los caracteres posibles
  char caracteres[4] = {'L', 'C', 'D', 'E'};
  // Se recorre el arreglo
  for (int i = 0; i < 4; i++) {
    // si el método no se encuentra retorna falso
    if (caracteres[i] == metodo_colision) {
      return false;
    }
  }
  return true;
}

/* Función que imprime el arreglo */
void imprimir_arreglo(int arreglo[], int tamanio) {
  cout << endl;
  cout << "----------------------------------------" << endl;
  cout << "--- ARREGLO: (Dato, Posición) ---" << endl;
  // Se imprime el arreglo con un diseño facil elegido para la vision del usuario
  for (int i = 0; i < tamanio; i++) {
    if (arreglo[i] == 0) {
      cout << "(*, "<< i + 1 <<")" << (i < tamanio - 1 ? "-": "");
    } else {
      cout << "(" << arreglo[i] <<", " << i + 1 <<  ")" << (i < tamanio - 1 ? "-": "");
    }
  }
  cout << endl;
  cout << "----------------------------------------" << endl;
}

/* Función que llena el arreglo */
void rellenar_arreglo(int arreglo[], int tamanio) {
  for (int i = 0; i < tamanio; i++) {
    // Agregamos el arreglo con vacíos para hacer las condiciones cuando sean necesarias
    arreglo[i] = '\0';
  }
}

/* Función que retorna el número primo más cercano al tamaño */
int buscar_primo(int tamanio) {
  int primo = 0, aux = 0;
  for (int i = 2; i <= tamanio; i++) {
    aux = 0;
    for (int j = 1; j <= tamanio; j++) {
      // si i módulo de j es 0, incrementamos aux en 1.
      if (i % j == 0)
        aux++;
    }
    //si solo tiene dos números divisores entonces es primo y se guarda momentaneamente
    if (aux == 2) {
      primo = i;
    }
  }
  // Devuelve el primo más cercano al tamaño ingresado por el usuario
  return primo;
}

/* Función que retorna verdadero o falso según si el arreglo se encuentre lleno */
bool comprobar_lleno(int arreglo[], int tamanio) {
  // Recorremos el arreglo buscando a los datos distintos a vacio
  for (int i = 0; i < tamanio; i++) {
    if (arreglo[i] == '\0') {
      return false;
    }
  }
  return true;
}

/* Función que según la elección del metodo de colisión genera el hash*/
void prueba_insercion(int arreglo[], int tamanio, int numero, int primo, char metodo_colision, Lista * lista) {
  Hash * hash = new Hash();
  // De ser "l", Se ocupa prueba lineal
  if (metodo_colision == 'L') {
    hash -> prueba_lineal(arreglo, tamanio, numero, primo);
  }
  // De ser "c", Se ocupa prueba cuadratica
  if (metodo_colision == 'C') {
    hash -> prueba_cuadratica(arreglo, tamanio, numero, primo);
  }
  // De ser "d", Se ocupa prueba de doble dirección
  if (metodo_colision == 'D') {
    hash -> doble_direccion(arreglo, tamanio, numero, primo);
  }
  // De ser "e", Se ocupa encadenamiento
  if (metodo_colision == 'E') {
    hash -> encadenamiento(arreglo, tamanio, numero, primo, lista);
  }
}

/* Función principal del programa*/
int main(int argc, char * argv[]) {
  Lista * lista = new Lista();
  bool on = true;
  system("clear");
  // Obtenemos el metodo de colision que ingreso el usuario como parametro
  char metodo_colision;
  if (argc == 2) {
    // Guardamos el metodo
    metodo_colision = toupper(argv[1][0]);
  } else {
    // de faltar el parametro del metodo se da aviso al usuario y se cierra
    cout << "-- Se necesita 1 parametro --" << endl;
    return 1;
  }
  // Verifica si el método ingresado existe
  if (comprobar_metodo(metodo_colision)) {
    cout << "Ingrese un metodo valido" << endl;
    return 1;
  }
  string line;
  int tamanio = 0, numero = 0, primo = 0, posicion = 0;
  // Se pide el tamaño del arreglo que desea el usuario
  cout << "Ingrese el tamaño del arreglo" << endl;
  cout << "Tamaño: ";
  getline(cin, line);
  tamanio = stoi(line);

  // Funcion que nos devolverá el primo para generar el modulo de hash
  primo = buscar_primo(tamanio);
  // Creamos un arreglo con el tamaño del usuario
  int arreglo[tamanio];
  Hash * hash = new Hash();
  // Rellenamos el arreglo con datos que nos permiten comparar
  rellenar_arreglo(arreglo, tamanio);

  // Bucle principal del programa
  while (on) {
    // Obtenemos la opcion pedida por el usuario
    switch (menu()) {
    case 1:
      // De estár lleno el arreglo este no permite el ingreso de nuevos valores
      if (comprobar_lleno(arreglo, tamanio)) {
        cout << "El arreglo se encuentra lleno" << endl;
      }
      // De tener espacio se agregan
      else {
        // Capturamos numero
        cout << "Numero: ";
        getline(cin, line);
        numero = stoi(line);
        // Sacamos la posicion del numero
        posicion = hash -> modulo_hash(numero, primo);
        // Si no existe nada en esa posicion se asigna e imprime 
        if (arreglo[posicion] == '\0') {
          arreglo[posicion] = numero;
          imprimir_arreglo(arreglo, tamanio);
        }
        // De lo contrario existe colision y se debe ejecutar algun metodo
        else {
          cout << "Colision en posicion: " << posicion + 1 << endl;
          // Se llama a la prueba de insercion que contiene a los metodos y se imprime como queda
          prueba_insercion(arreglo, tamanio, numero, primo, metodo_colision, lista);
          imprimir_arreglo(arreglo, tamanio);
        }
      }
      break;
    case 2:
      // Mostramos como se encuentra el arreglo actualmente 
      cout << "El arreglo se encuentra de la siguiente forma" << endl;
      imprimir_arreglo(arreglo, tamanio);
      // Si arreglo no esta vacio entonces se puede buscar
      if (comprobar_lleno(arreglo, tamanio)) {
        // Capturamos el numero
        cout << "Ingrese numero: ";
        getline(cin, line);
        numero = stoi(line);
        // Obtencion de posicion con Hash
        posicion = hash -> modulo_hash(numero, primo);
        // Si se encuentra el numero en esa posicion se imprime 
        if (arreglo[posicion] == numero) {
          cout << "Clave se encuentra en posicion: " << posicion + 1 << endl;
          imprimir_arreglo(arreglo, tamanio);
        }
        // Sino debemos ejecutar una prueba de insercion para agregar el numero
        else {
          prueba_insercion(arreglo, tamanio, numero, primo, metodo_colision, lista);
          imprimir_arreglo(arreglo, tamanio);
        }
      }
      // Sino el arreglo está vacio
      else {
        cout << "El arreglo se encuentra vacio" << endl << endl;
      }
      break;
    case 3:
      cout << "Finalización del programa con éxito" << endl;
      on = false;
      break;
    default:
      cout << "ingrese una opción valida" << endl;
      break;
    }
  }
  return 0;
}
